# UFL - Unofficial Fedora Labs 29.1.2

## All variant use KDE Desktop

> The computer on which you intend to install UFL 29 from the live CD should have:
> * a DVD or USB drive (minimum 4Gib), and the capability to boot from this drive.
> * a 1 GHz processor or faster
> * at least 2 GB of memory (RAM)

## Download

Download from OSDN Server japan.
- [Home](https://osdn.net/projects/ufl/downloads/70819/UFL-Home_KDE-Live-29.1.2.iso/)
- [Design Suite Edition](https://osdn.net/projects/ufl/downloads/70819/UFL-Design_suite_KDE-Live-29.1.2.iso/)
- [Developer Edition](https://osdn.net/projects/ufl/downloads/70819/UFL-Developer-Live-29.1.2.iso/)

## Featured

### Home Edition

1. KDE Apps
  - rss: akregator
  - irc: konversation
  - parted: kde-partitionmanager
  - gdrive: kio-gdrive
  - mtp: kio_mtp
  - support wacomtablet: kcm_wacomtablet
  - bluetooth: bluez-obexd
  - image viewer: gwenview
  - thumbnail: ffmpegthumbs
  - video thumbnail: kffmpegthumbnailer
  - encrypt: kleopatra, sirikali
  - text editor: kwrite
  - downloader & torrent: kget
  - disc burner: k3b
  - image editing: kolourpaint
  - music player: amarok
  - video player: dragon
  - password manager: kwalletmanager
  - backup: kbackup
  - disk free: kdf
  - rename: krename
  - systemlog: ksystemlog
  - keyboard monitor: key-mon
  - scan image: skanlite
  - converter: qwinff
  - cleaner: sweeper
  - record Desktop: simplescreenrecorder
  - remote desktop: krdc
  - desktop sharing: krfb
  - virtual machine: aqemu
  - virtual keyboard: kvkbd
  - bootable: isoimagewriter
2. Rpmfusion

3. Codec support

4. Browser: firefox

5. Office: libreoffice

6. Basic design: gimp, inkscape

7. Compression tool

8. Tweak /etc/skel

9. Google noto font

10. Video downloader

11. Cli editor vim/neovim

12. SSG: Hugo

13. Mtp android

14. Printer support

15. Livecd-tools

### Design_Suite Edition

Home design plus :

1. inkscape plugin:
  - inkscape
  - inkscape-psd
  - inkscape-sozi
  - inkscape-table

2. gimp plugin:
  - gimp
  - gimpfx-foundry
  - gimp-paint-studio
  - gimp-dds-plugin 
  - gimp-fourier-plugin 
  - gimp-gap
  - gimp-high-pass-filter
  - gimp-layer-via-copy-cut
  - gimp-lensfun
  - gimp-lqr-plugin
  - gimp-normalmap
  - gimp-paint-studio
  - gimp-resynthesizer
  - gimp-save-for-web
  - gimp-separate+
  - gimp-wavelet-decompose
  - gmic-gimp

3. video editor: kdenlive + frei0r-plugins

4. layout: scribus

5. painting: krita

6. 2d animation: synfigstudio

7. pattern editor: kxstitch

8. photographer: darktable

9. audio editor: kwave

10. transcoding tool: avidemux-qt

11. prefesional photo management: entangle, digikam

12. lightroom: darktable

13. font editing: fontforge

14. 3d animation: blender

15. optimal png: optipng

16. color extras: colord-extra-profiles

17. fonts from design-suite:
  - aajohan-comfortaa-fonts
  - julietaula-montserrat-fonts
  - lato-fonts
  - open-sans-fonts
  - overpass-fonts

### Developer Edition

Home design plus :

1. IDEs:
  - python: ninja-ide
  - c/c++: kdevelop
  - qt: qt-creator, qt5-designer
  - good code: krazy2

2. qt devel depedencies

3. devel tools:
  - development-tools
  - development-libs
  - c-development
  - rpm-development-tools
  - C++ libraries
4. version control: git, mercurial

5. c:
  - gcc
  - clang
  - c++
  - autotools
  - cmake
  - static analysis

6. python:
  - @python-classroom
  - @python-science
  - django
  - python 2 tools/libraries
  - python 3 and tools/libraries
  - matplotlib backends

7. ruby:
  - cruby
  - jruby
  - gems
  - bundler (gem install bundler)
  - ruby on rails (or gem install rails)
  - sinatra (or gem install sinatra)
  - ruby on rails

8. databases :
  - mongodb
  - sqlite
  - postgresql
  - mariadb sql
  - cassandra

9. rpm packaging

