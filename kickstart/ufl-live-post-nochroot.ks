%post --nochroot
# standard chroot

cp -fr  recipes/plymouth/* \
        $INSTALL_ROOT/

cp -fr  recipes/skel-default/* \
        $INSTALL_ROOT/

cp -fr  recipes/skel-default-root/root/ \
        $INSTALL_ROOT/

%end
